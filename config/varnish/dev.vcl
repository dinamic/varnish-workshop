# entry point for development environment with debug information
vcl 4.0;

backend default {
    .host = "127.0.0.1";
    .port = "8080";
}

include "common.vcl";

sub vcl_backend_response {
   set beresp.http.X-TTL = beresp.ttl;
}

sub vcl_deliver {
   if (obj.hits > 0) {
      set resp.http.X-Cache = "HIT";
   } else {
      set resp.http.X-Cache = "MISS";
   }
}

sub vcl_backend_response {
    # Disable default grace (10s) for expiration exercise
    set beresp.grace = 0s;
}
