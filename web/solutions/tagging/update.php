<?php
require('../../../vendor/autoload.php');
require('init.php');

use FOS\HttpCache\CacheInvalidator;
use FOS\HttpCache\ProxyClient\Varnish;

if ('POST' !== $_SERVER['REQUEST_METHOD']) {
    die('Go to ./');
}

$toInvalidate = array();
foreach (array_keys($items) as $key) {
    if (isset($_POST[$key])) {
        $items[$key] = $_POST[$key];
        $toInvalidate[] = $key;
    }
}

file_put_contents(DATAFILE, serialize($items));

if (count($toInvalidate)) {
    $client = new Varnish(array('127.0.0.1'), 'varnish.lo');
    $invalidator = new CacheInvalidator($client);

    $invalidator
        ->invalidateTags($toInvalidate)
        ->flush()
    ;
}

header('Location: http://varnish.lo' . dirname($_SERVER['PHP_SELF']));

