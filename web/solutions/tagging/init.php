<?php

const DATAFILE = '/tmp/tagging';

if (file_exists(DATAFILE)) {
    // Items stored from a previous POST
    $items = unserialize(file_get_contents(DATAFILE));
} else {
    // Default items before an update has been POSTed
    $items = array(
        'applepie' => 'A delicious applie pie',
        'applejuice' => 'Juicy fruit juice',
        'orangejuice' => 'Another flavour of juice',
    );
}
