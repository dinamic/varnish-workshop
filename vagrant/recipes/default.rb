# install necessary software
%w(
vim
apache2
git
php5
libapache2-mod-php5
php5-cli
php5-curl
php5-sqlite
php5-pgsql
php5-intl
apt-transport-https
).each { | pkg | package pkg }

# configure Varnish repo
bash "add Varnish repo" do
  user "root"
  not_if "test -f /etc/apt/sources.list.d/varnish-cache.list"
  code <<-EOC
    curl https://repo.varnish-cache.org/GPG-key.txt | apt-key add -
    echo "deb https://repo.varnish-cache.org/debian/ wheezy varnish-4.0" >> /etc/apt/sources.list.d/varnish-cache.list
    apt-get update
  EOC
end

package "varnish" do
  action :install
end

# copy .bashrc from template
template "/home/vagrant/.bashrc.git" do
  user "vagrant"
  group "users"
  mode "0644"
  source ".bashrc.git.erb"
end

execute "load bashrc" do
  user "vagrant"
  not_if "egrep 'bashrc.git' /home/vagrant/.bashrc"
  command "echo 'source /home/vagrant/.bashrc.git' >> /home/vagrant/.bashrc"
end


# copy virtual hosts file from template
template "/etc/apache2/sites-available/default" do
  mode "0644"
  source "vhost.conf.erb"
  #notifies :reload, "service[apache2]"
end

bash "modify timezone" do
  code <<-EOC
    sudo cp /etc/timezone /etc/timezone_old
    echo "Europe/Zurich" > /etc/timezone
    dpkg-reconfigure -f noninteractive tzdata
  EOC

  creates "/etc/timezone_old"
end

# set the timezone for php
execute "date.timezone = Europe/Zurich in php.ini?" do
  user "root"
  not_if "test -f /etc/php5/conf.d/10-timezone.ini"
  command "echo 'date.timezone = Europe/Zurich' > /etc/php5/conf.d/10-timezone.ini"
end

bash "have apache listen on port 8080" do
  user "root"
  not_if "grep 'Listen 8080' /etc/apache2/ports.conf"
  code <<-EOH
    sed -i 's/Listen 80/Listen 8080/g' /etc/apache2/ports.conf
    sed -i 's/NameVirtualHost \\*:80/NameVirtualHost *:8080/g' /etc/apache2/ports.conf
  EOH
  notifies :restart, "service[apache2]"
end

bash "retrieve composer" do
  user "vagrant"
  cwd "/vagrant"
  code <<-EOH
  set -e

  # check if composer is installed
  if [ ! -f /usr/local/bin/composer ]
  then
    curl -sS https://getcomposer.org/installer | php
    sudo mv composer.phar /usr/local/bin/composer
  else
    sudo /usr/local/bin/composer selfupdate
  fi
  EOH
end

bash "run composer" do
  user "vagrant"
  cwd "/vagrant"
  code <<-EOH
  set -e
  export COMPOSER_HOME=/home/vagrant
  composer install --dev --no-interaction
  EOH
end

# copy Varnish config template
template "/etc/default/varnish" do
  user "root"
  source "varnish.erb"
  notifies :restart, "service[varnish]"
end

bash "link varnish configuration" do
  user "root"
  not_if "test -L /etc/varnish/default.vcl"
  code <<-EOH
      rm /etc/varnish/default.vcl
      ln -s /vagrant/config/varnish/dev.vcl /etc/varnish/default.vcl
      ln -s /vagrant/config/varnish/* /etc/varnish/
  EOH
  notifies :restart, "service[varnish]"
end

# Start and enable Apache
service "apache2" do
  supports :restart => true, :reload => true, :status => true
  action [ :enable, :start ]
end

# Start Varnish
service "varnish" do
  supports :restart => true, :status => true
  action [ :enable, :start ]
end
